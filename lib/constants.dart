import 'package:flutter/material.dart';

const kInputStyle = TextStyle(
  fontSize: 50,
  color: Colors.white,
);

const kResultStyle =
    TextStyle(fontSize: 40, color: Colors.white, fontWeight: FontWeight.w500);
