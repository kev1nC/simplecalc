import 'dart:math';

double add(double num1, double num2) {
  return num1 + num2;
}

double sub(double num1, double num2) {
  return num1 - num2;
}

double mul(double num1, double num2) {
  return num1 * num2;
}

double div(double num1, double num2) {
  return num1 / num2;
}

double mod(double num1, double num2) {
  return num1 % num2;
}

double roundDouble(double value, int places) {
  double mod = pow(10.0, places);
  return ((value * mod).round().toDouble() / mod);
}

int getprececdence(String input) {
  switch (input) {
    case "+":
      return 4;
      break;
    case "-":
      return 4;
      break;
    case "*":
      return 5;
      break;
    case "/":
      return 5;
      break;
    case "mod":
      return 5;
      break;
    case "(":
      return 6;
      break;
    case ")":
      return 6;
      break;
  }
}

bool isDouble(String number) {
  if (int.tryParse(number) == null) {
    try {
      if (double.parse(number).isFinite) return true;
    } catch (e) {
      return false;
    }
  }
  return false;
}

// bool isDouble(String number) {
//   try {
//     double n = double.parse(number);
//     if (n % 1 == 0) {
//       return false;
//     } else {
//       return true;
//     }
//   } catch (e) {
//     return false;
//   }
// }

bool isOperator(String input) {
  switch (input) {
    case "+":
      return true;
      break;
    case "-":
      return true;
      break;
    case "*":
      return true;
      break;
    case "/":
      return true;
      break;
    case "mod":
      return true;
      break;
    default:
      return false;
      break;
  }
}

bool isDecimal(String input) {
  if (input == ".") {
    return true;
  } else {
    return false;
  }
}
