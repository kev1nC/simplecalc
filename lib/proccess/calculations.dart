import 'package:validators/validators.dart'; // for the isNumeric function
import 'package:stack/stack.dart' as st;
import 'operations.dart';

double result;
double round;
double num1;
double num2;
String ope;
int flag = 0;
List<String> numList = [];

String displayInputs;
String number = "";
bool negative = false;
bool decimal = false;
int openParen = 0;
int closeParen = 0;

String expression = "valid";
List<String> container = []; // an array that will reversed the postfix exp

List<String> allInputs =
    []; // array of inputs, just to check every input by the user
st.Stack<String> nums = st.Stack(); // number values stack for computation
st.Stack<String> fin = st.Stack(); // postfix stack
st.Stack<String> ops = st.Stack(); // operators stack

// pmdas - paren, multiply, divide, add, sub
calculation(String input) {
  // clear everthing
  if (input == "C") {
    allInputs = [];
    container = [];
    number = "";
    num1 = 0;
    num2 = 0;
    ope = "";
    closeParen = 0;
    openParen = 0;
    displayInputs = null;
    result = null;
    nums = new st.Stack();
    fin = new st.Stack();
    ops = new st.Stack();
    return;
  }

  // if ⌫ is pressed we delete the last input and reinitialize the displayinputs
  if (input == "del" && allInputs.isNotEmpty) {
    allInputs = allInputs.sublist(0, allInputs.length - 1);
    displayInputs = allInputs.join();
    return;
  }
  // if the input is a number
  if (isNumeric(input)) {
    if (negative == true) {
      number = "-" + number + input.toString();
      negative = false;
      expression = "valid";
    } else if (input == "0" && allInputs.isEmpty) {
      // if the input is just zero, do nothing
      return;
    } else {
      // if it is positive integer
      number = number + input.toString();
      expression = "valid";
    }
  } else if (input == ".") {
    // if the first input is .
    if (result != null) {
      return;
    }
    // this is to check if the last input is again a "."
    if (allInputs.isNotEmpty == true) {
      if (isDecimal(allInputs[allInputs.length - 1]) == true) {
        // do nothing if it is a multiple .
        return;
      } else {
        // if not, then merge the number to the "."
        number = number + input.toString();
        decimal = false;
      }
    } else if (result == null) {
      // this is for the 2 "." 89 , decimal point
      number = number + input.toString();
      decimal = true;
    }
  } else {
    // if an operator is pressed
    // check if there is a whole and valid number that the user pressed before pushing it on the fin stack
    if (number != "") {
      fin.push(number.toString());
      number = "";
      negative = false;
    }

    //check if the user pressed on minus without any leading numbers
    if (allInputs.isEmpty == true) {
      if (input == "-") {
        negative = true;
      } else if (input != "(") {
        return;
      }
    }

    // check if the input and the last input are operators
    if (isOperator(input) == true) {
      if (allInputs.isNotEmpty == true) {
        // this makes the operator to change when another operator is last pressed
        // so to make a computation with - number, we are going to check if the last pressed number is equal to (
        // if yes, then make negative true, so that the next number will be negative

        if (isOperator(allInputs[allInputs.length - 1]) == true) {
          ops.pop();
          ops.push(input);
          allInputs = allInputs.sublist(0, allInputs.length - 1);
          expression = "invalid";
        }
        if (input == "-" && allInputs[allInputs.length - 1] == "(") {
          negative = true;
          expression = "invalid";
        }
      }
    }

    // check if the user pressed ( after pressing - sign

    // if the expression is valid, meaning no duplicated operators in a row, no "." in a row
    if (expression == "valid") {
      // if it's an operator and didn't saw any negative number yet and not an equal sign
      if (input != '=' && negative == false) {
        // if there is a result, initialize all inputs to the result + the operator that the user pressed
        // this is if there is a recent result. this will make that recent result, be pushed in a initialized postfix stack
        if (nums.size() != 0) {
          allInputs = [];
          fin = st.Stack();
          fin.push(result.toString());
          ops.push(input);
          displayInputs = result.toString() + input.toString();
          allInputs.add(result.toString());
          allInputs.add(input);
          container = [];
          number = "";
          result = null;
          nums.pop();
          flag = 1;
          return;
        }

        // check if the user is pressing open parentheses
        if (input == "(") {
          openParen++;
        }
        // this is when a user has already pushed something in the operator stack
        if (ops.isNotEmpty) {
          int inp = getprececdence(input);
          int top = getprececdence(ops.top());

          if (inp == top) {
            // here, we check if the
            // TODO : yo uare solving here
            if (input == ")" && ops.top() == "(") {
              closeParen++;
              ops.pop();
              if (ops.isEmpty && fin.isEmpty) {
                displayInputs = "Syntax Error";
                allInputs = [];
                closeParen = 0;
                openParen = 0;

                return;
              }
            } else {
              //check the top of the operator stack and current if they are the same (
              //TODO: here you need to check for a nested operation
              if (input == "(" && ops.top() == "(") {
                ops.push(input);
              } else {
                //pop here, but check if after popping the top is equal to the input (current)
                fin.push(ops.top());

                ops.pop();
                //checking part
                // if the input operator is equal to the top of the stack, if it is equal, pop and pushed it to the postfix stack
                // if not, just push the input to the top of the operator stack
                // if for the first time, the operator stack is empty, so just push the input
                if (ops.size() != 0) {
                  inp = getprececdence(input);
                  top = getprececdence(ops.top());
                  if (inp == top) {
                    fin.push(ops.top());
                    ops.pop();
                    ops.push(input);
                  } else {
                    ops.push(input);
                  }
                } else {
                  if (input != "(") {
                    ops.push(input);
                  }
                }
              }
            }
          } else if (inp > top) {
            // if the input's precedence is bigger than the operator stack's top precedence
            // just pushing the input to the ops stack if it is an operator
            // if the current is ), pop until you see (, this means that there is a parenthesis expression earlier

            if (input == ")") {
              closeParen++;
              // if close paren is more than open paren, we immediately do a syntax error
              if (closeParen > openParen) {
                displayInputs = "Syntax Error";
                allInputs = [];
                return;
              }
              while (ops.top() != "(") {
                fin.push(ops.top());
                ops.pop();
              }
              ops.pop();
            } else {
              // if it's not yet the end of the parenthesis, push the input to the operator stack
              ops.push(input);
            }
          } else if (inp < top) {
            //pop here, but check if after popping the top is equal to the inp
            // before pushing to the fin, check if it is a parenthesis. if it is a parenthesis ( dont push it to the fin
            if (ops.top() == "(") {
              // if the input is (, this means that we have a parenthesis expression
              if (input == "-" && allInputs.isEmpty == true ||
                  input == "-" && allInputs[allInputs.length - 1] == "(") {
                //in here, if the user pressed something like, (-34+2)
                // we flag the negative as true so that in the event that the user presses something again
                // we can be able to check for it
                negative = true;
              } else {
                // if the user pressed a normal operator then just push it in the operator stack
                // this is just a normal minus sign for subtraction
                ops.push(input);
              }
            } else {
              // if the user pressed some normal operator, we then pop the top of the operator stack if the input's precedence
              // is lower than that of the top of the operator stack
              fin.push(ops.top());
              ops.pop();
              // we check the precedence not the equal to input
              // again in here, after popping the top of the operator stack, let's check if the next top of the operator
              // stack is the same with the input
              // same as the arguement above, if it's the first time pressing an operator, just push it to the stack
              if (ops.size() != 0) {
                inp = getprececdence(input);
                top = getprececdence(ops.top());
                if (inp == top) {
                  fin.push(ops.top());
                  ops.pop();
                  ops.push(input);
                } else {
                  ops.push(input);
                }
              } else {
                ops.push(input);
              }
            }
          }
        } else {
          // if the user pressed an operator for the first time, push it on top of the operator stack
          // initialize the stack ( the stack is still empty )
          // checking if the user pressed something that is not on the rule
          // for example, user pressed ) this symbol without any leading (
          if (input == ")") {
            displayInputs = "Syntax Error";
            allInputs = [];
            closeParen = 0;
            openParen = 0;
            return;
          }
          ops.push(input);
        }
      } else {
        //pop and push ops stack until end, because there are some operators that are left inside the ops stack, if none ,the while
        // loop is not used
        while (ops.size() != 0) {
          if (ops.top() == "(") {
            ops.pop();
          } else {
            fin.push(ops.top());
            ops.pop();
          }
        }
      }
    }
  }

  if (input == "=") {
    // if the user has a valid expression and pressed equal sign
    if (result != null) {
      // since we are calculating something, if the user has a recent result and the user is pressing equal sign
      // we dont do anything
      return;
    }
    // check if user presses equals with a syntax error
    if (displayInputs == "Syntax Error") {
      displayInputs = null;
      allInputs = [];
      closeParen = 0;
      openParen = 0;
      return;
    }
    // check if the number of close paren and open paren is the same'
    if (closeParen != openParen) {
      displayInputs = "Syntax Error";
      allInputs = [];
      closeParen = 0;
      openParen = 0;
      return;
    }
    // here you we have the fin stack (postfix stack).
    // we put it inside a container  (top , pop method)
    while (fin.size() != 0) {
      // check if the top of the fin is an operator, if the next value is also an operator, then syntax error

      container.add(fin.top());

      fin.pop();
    }
    container = container.reversed.toList(); // got the reversed
    print(container); //printing this for reference and debugging
    //traverse the array here
    int i = 0;
    double computed;
    // num tester;
    while (i <= container.length - 1) {
      // traversing the array one by one
      if (isNumeric(container[i]) || isDouble(container[i]) == true) {
        if (!isOperator(container[i])) {
          nums.push(container[i].toString());
        }
      } else {
        // must be an operator
        // get the operator in the index
        ope = container[i];
        // get the top and pop the nums stack
        num2 = double.parse(nums.top());
        nums.pop();
        num1 = double.parse(nums.top());
        nums.pop();
        // check the operator then call the function for that operator

        switch (ope) {
          case "+":
            computed = add(num1, num2);
            break;
          case "-":
            computed = sub(num1, num2);
            break;
          case "*":
            computed = mul(num1, num2);
            break;
          case "/":
            computed = div(num1, num2);
            break;
          case "mod":
            computed = mod(num1, num2);
            break;
        }
        nums.push(computed
            .toDouble()
            .toString()); // pushing the computed value, just in case it is not yet the end of the array
      }
      i++;
    }
    round = double.parse(nums
        .top()); // parsing the top of the nums (number values) stack to a double

    // catching the error if the number is NaN, if it's a nunmber just do the normal assignment
    try {
      round = double.parse(nums.top());
      result = roundDouble(
          round, 8); // rounding the computed value into 8 decimal places
      return;
    } catch (e) {
      result = round;
      return;
    }
  }
  //gets all the input and store it inside the displayInputs and it will be displayed on the calculator.dart
  allInputs.add(input);
  displayInputs = allInputs.join();
}
