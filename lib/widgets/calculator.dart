import 'package:flutter/material.dart';
import 'package:simplecalc/constants.dart';
import 'package:simplecalc/proccess/calculations.dart';
import 'buttons.dart';
import 'package:auto_size_text/auto_size_text.dart';

class Calculator extends StatefulWidget {
  @override
  _CalculatorState createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      // displayInputs == null ? "" : "$displayInputs",
                      result == null ? "" : "$result",
                      style: kResultStyle,
                    ),
                    Align(
                      child: AutoSizeText(
                        displayInputs == null ? "" : "$displayInputs",
                        // displayInputs = "tangina",
                        style: kInputStyle,
                        maxLines: 1,
                      ),
                      alignment: Alignment.bottomRight,
                    ),
                  ],
                ),
              ),
            ),
            Row(
              children: [
                CalculatorButton(
                    text: "C",
                    press: () {
                      setState(() {
                        calculation("C");
                      });
                    }),
                CalculatorButton(
                    text: "(",
                    press: () {
                      setState(() {
                        calculation("(");
                      });
                    }),
                CalculatorButton(
                    text: ")",
                    press: () {
                      setState(() {
                        calculation(")");
                      });
                    }),
                CalculatorButton(
                    text: "%",
                    press: () {
                      setState(() {
                        calculation("mod");
                      });
                    }),
                CalculatorButton(
                    text: "⌫",
                    press: () {
                      setState(() {
                        calculation("del");
                      });
                    }),
              ],
            ),
            Row(
              children: [
                CalculatorButton(
                    text: "7",
                    press: () {
                      setState(() {
                        calculation("7");
                      });
                    }),
                CalculatorButton(
                    text: "8",
                    press: () {
                      setState(() {
                        calculation("8");
                      });
                    }),
                CalculatorButton(
                    text: "9",
                    press: () {
                      setState(() {
                        calculation("9");
                      });
                    }),
                CalculatorButton(
                    text: "+",
                    press: () {
                      setState(() {
                        calculation("+");
                      });
                    }),
              ],
            ),
            Row(
              children: [
                CalculatorButton(
                    text: "4",
                    press: () {
                      setState(() {
                        calculation("4");
                      });
                    }),
                CalculatorButton(
                    text: "5",
                    press: () {
                      setState(() {
                        calculation("5");
                      });
                    }),
                CalculatorButton(
                    text: "6",
                    press: () {
                      setState(() {
                        calculation("6");
                      });
                    }),
                CalculatorButton(
                    text: "-",
                    press: () {
                      setState(() {
                        calculation("-");
                      });
                    }),
              ],
            ),
            Row(
              children: [
                CalculatorButton(
                    text: "1",
                    press: () {
                      setState(() {
                        calculation("1");
                      });
                    }),
                CalculatorButton(
                    text: "2",
                    press: () {
                      setState(() {
                        calculation("2");
                      });
                    }),
                CalculatorButton(
                    text: "3",
                    press: () {
                      setState(() {
                        calculation("3");
                      });
                    }),
                CalculatorButton(
                    text: "*",
                    press: () {
                      setState(() {
                        calculation("*");
                      });
                    }),
              ],
            ),
            Row(
              children: [
                CalculatorButton(
                    text: "0",
                    press: () {
                      setState(() {
                        calculation("0");
                      });
                    }),
                CalculatorButton(
                    text: ".",
                    press: () {
                      setState(() {
                        calculation(".");
                      });
                    }),
                CalculatorButton(
                    text: "/",
                    press: () {
                      setState(() {
                        calculation("/");
                      });
                    }),
                CalculatorButton(
                    text: "=",
                    press: () {
                      setState(() {
                        calculation("=");
                      });
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
